// JAS ARRAY MANIPULATION 88888888888888888888
let grades =[98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];
// OR you can do this like this:
let myTasks=[
'drink HTML',
'eat Javacript',
'inhale CSS',
'bake SASS'
];
/*INDEX: n-1 where n is the total length of array 
e.g. 
grades[0] that is 98.5*/
// --------------------------------------------
// REASSIGNING ARRAY
console.log("Array before reassignment");
console.log(myTasks);

myTasks[0]="clean Node";
console.log('Array after reassignment');
console.log(myTasks);
// -------------------------------------------
// READING FROM ARRAY
console.log(grades[0]);
console.log(computerBrands[0][0]);
// ------------------------------------------
// ACCESSING ELEMENT THAT DOES NOT EXISTS
console.log(grades[20]);
// -----------------------------------------
// LENGTH OF ARRAY : (.length)
console.log(computerBrands.length);


// MANIPULATION ARRAYS  888888888888888888888888

// ARRAY METHODS:
/*===============================================
1- MUTATOR METHOD- functions that mutate/change an array after they're created */

let fruits = ['Apple', 'orange', '1Kiwi', 'Dragon Fruit'];
/*>>  .push() - adds an element at the end and returns the array length
	SYNTAX:  arrayName.push(newElement)     */
console.log('Current Array')
console.log(fruits);
let fruitsLength= fruits.push('Mango');
console.log('Mutated array from .push() method');
console.log(fruits);
// add more elements:
fruits.push('Avocado', 'Guava');
console.log('Mutated array from multiple .push() method');
console.log(fruits);

/*>>  .pop() - removes an element at the end and returns the removed element
	SYNTAX:  arrayName.pop()     */
let removedfruit =fruits.pop();
console.log(removedfruit);
console.log('Mutated array from .pop() method');
console.log(fruits);

/*>>  .unshift() - adds element/s at the beginning 
	SYNTAX:  arrayName.unshift(newElement)     */
fruits.unshift('Lime', 'banana');
console.log('Mutated array from .unshift() method');
console.log(fruits);

/*>>  .shift() - removes element at the beginning returns the removed element 
	SYNTAX:  arrayName.shift()     */
let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log('Mutated array from .shift() method');
console.log(fruits);

/*>>  .splice() - removes element/s from a specified index num. and adds elements
	SYNTAX:  arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)     */
fruits.splice(1,2,'Lime', 'Cherry');
console.log('Mutated array from .splice() method');
console.log(fruits);

/*>>  .sort() - rearranges the element
	SYNTAX:  arrayName.sort()     */
fruits.sort();
console.log('Mutated array from .sort() method');
console.log(fruits);

/*>>  .reverse() - reverses the order the element 
	SYNTAX:  arrayName.reverse()     */
fruits.reverse();
console.log('Mutated array from .reverse() method');
console.log(fruits);

/*=================================================
2- NON-MUTATOR METHOD- functions that DO NOT modify/change an array after they're created */
let countries =['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];

/*>>  .indexOf() - returns the index no. of the 1st matching element in array 
	SYNTAX:  arrayName.indexOf(elementName)     */
let firstIndex=countries.indexOf('PH');
console.log('Result of .indexOf(PH):' + firstIndex);

let invalidCountry=countries.indexOf('BR');
console.log('Result of .indexOf():' + invalidCountry);

/*>>  .lastIndexOf() - returns the index no. of the LAST matching element in array 
	SYNTAX:  arrayName.lastIndexOf(elementName)     */
let lastIndex=countries.lastIndexOf('PH');
console.log('Result of .lastIndexOf(PH):' + lastIndex);

/*>>  .slice() - portions/slices elements and returns/makes a new array 
	SYNTAX:  arrayName.slice(startingIndex) 
			 arrayName.slice(startingIndex, endingIndex)    
	NOTE: endingEndex - that no. of index is NOT included in the new array */
console.log('>>>NON-Mutated array from .slice() method');
console.log(countries);

let slicedArrayA=countries.slice(2);
// all index after 2 are ALL included in the new array
console.log('Result of .slice(2):' + slicedArrayA);
let slicedArrayB=countries.slice(2,5);
// index 5 is NOT included in the new array
console.log('Result of .slice(2,5):' + slicedArrayB);

/*>>  .toString() - returns an array as a String separataed by COMMAS (PH,GH,AH) 
	SYNTAX:  arrayName.toString()     */
let stringArray=countries.toString('PH');
console.log('>>>Result of .toString():');
console.log(stringArray);

/*>>  .concat() - combines 2 arrays in one 
	SYNTAX:  arrayName1.concat(arrayName2)     */

let tasksArrayA= ['drink HTML', 'eat Js'];
let tasksArrayB= ['inhale CSS', 'breathe SASS'];
let tasksArrayC= ['get GIT', 'be NODE'];

let tasks=tasksArrayA.concat(tasksArrayB);
console.log('Result of  tasksArrayA.concat(tasksArrayB):');
console.log(tasks);
// combine multiple arrays:
let alltasks=tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log('Result of  tasksArrayA.concat(tasksArrayB, tasksArrayC):');
console.log(alltasks);
// combinig arrays with elements:
let combinedTasks = tasksArrayA.concat('smell EXPRESS', 'throw REACT');
console.log('Result of  combined arrays with elements:');
console.log(combinedTasks);

/*>>  .join() - returns an array as a string separated by specified separator string (a,b,c,d)
	SYNTAX:  arrayName.join("separatorString")     */
let users =['John','Cat','Rin','Jin']
console.log(">>>Result of  .join():");
console.log(users.join());

console.log(">>>Result of  .join('withseparators'):");
console.log(users.join(""))
console.log(users.join('-'))


/*=================================================
3- ITERATION METHOD- loops designed to perform repetitive tasks on arrays 
	-use singular form for parameters*/

/*>>  .forEach() - similar to FOR LOOP that iterates on each array. each element is display in their own line. 
	SYNTAX:  arrayName.forEach(function(indivElementparameter){statement})     */
console.log('>>>Results of .forEach() with parameter task:')
alltasks.forEach(function(task){
	console.log(task);
});
// using forEach with conditional statement:
let filteredTasks =[];

alltasks.forEach(function(task){
	if(task.length >10){
		filteredTasks.push(task)
	}
});
console.log('Result of filtered tasks:')
console.log(filteredTasks);
// above means that if the characters of the element is >10, then it will be displayed result:breathe SASS

/*>>  .map() - itirates on each element and returns a new array with different values 
		SYNTAX:  
		let/const resultArray= arrayName.map(function (indivElementparameter){return statement/s})     */
console.log('>>>Using .map():')

let numbers = [1,2,3,4,5];
let numberMap =numbers.map(function(number){
	return number * number;
});
console.log('Result of .map() number1:');
console.log(numberMap);


/*>>  .every() - checks if ALL elements in an array meet the given condition. display :true or false
		SYNTAX:  
		let/const resultArray= arrayName.every(function (indivElementparameter){return statement/s})     */
console.log('>>>Using .every():')
let allValid = numbers.every(function(number){
	return (number <3);
});
console.log('Result of .every() if all elements is <3:');
console.log(allValid);

/*>>  .some() - checks if AT LEAST ONE in an array meet the given condition. display :true or false
		SYNTAX:  
		let/const resultArray= arrayName.some(function (indivElementparameter){return statement/s})     */

console.log('>>>Using .some():')
let someValid = numbers.some(function(number){
	return (number <3);
});
console.log('Result of .some() if one of the elements is <3:');
console.log(someValid);

/*>>  .filter() - return a new array that contains elements which meets the given condition
		SYNTAX:  
		let/const resultArray= arrayName.filter(function (indivElementparameter){return expression/condition})     */
console.log('>>>Using .filter():')

let filterValid= numbers.filter(function(number){
	return (number <3);
})
console.log('Result of .filter() filters elements that is <3:');
console.log(filterValid);

// ---------------------------------------------
let nothingFound= numbers.filter(function(number){
	return (number ==0);
})
console.log('Result of .filter() filters elements that is ==0:');
console.log(nothingFound);

// Filtering using forEach:-------------------------
// let numbers = [1,2,3,4,5];
let filteredNumbers =[];
numbers.forEach(function(number){
	if(number >3){
		filteredNumbers.push(number);
	}
})
console.log('Result of filtered elements (>3) that uses forEach');
console.log(filteredNumbers);

/*>>  .includes() - the result of the first method is used on the second method untill all "chained" methods are resolved
-filters an element base on what is inside the ( )
		SYNTAX:  
		.includes('')     */

let products= ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];
// changed to lowercase then filter elements with a:
let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes('a');
})
console.log('Result of filtered elements using .includes()');
console.log(filteredProducts);

/*>>  .reduce() - evaluates elements from left-right & returns/reduc the array into a single value
		SYNTAX:  
		let/const resultArray= arrayName.reduce(function (accumulator,currentValue){
		return expression/operation})   

accumulator -starts with the 1st elelemnt in array, accumulates result of each iteration
currentVALUE- starts with the second element and moves to the last */

// let numbers = [1,2,3,4,5];-------------------------------
// in short 1+2+3+4+5 reducedArray=15 OR
/* x=1 + y=2 ==3 
   x=3 + y=3 ==6
   x=6 + y=4 ==10
   x=10 + y=5 ==15 this is the last  coz 5 is the last element
*/
console.log('>>>Result of .reduce() method');
let iteration=0
let reducedArray = numbers.reduce(function(x,y){
	console.warn('current interation:' + ++iteration);
	console.log('accumulator:' + x);
	console.log('currentVALUE:'+ y);
	return x+y;
})
console.log('>>>Result of .reduce() on [1,2,3,4,5]');
console.log(reducedArray);

// reducing string arrays-----------------
let list =['Hello', 'Again' , 'World'];
let reducedJoin = list.reduce(function(x,y){
	return x + " "+ y;
});
console.log('>>>Result of .reduce() on strings');
console.log (reducedJoin);

// MULTIDIMENSIONAL ARRAY 888888888888888888888888
// ===============================================
// TWO DIMENSIONAL ARRAY- array within an array

let oneDim =[];
// 1st Dim  [     0         1   ]  
// 2nd Dim  [ [ 0  1 ]  [ 0  1 ]]
let twoDim =[[2,4], [6,8]];
// access element  : arrayName[1stDim] [2ndDim]
console.log('>>>accessing element in 2 dimensional array (2x2):');
console.log(twoDim[1][0]);
console.log(twoDim[0][1], + twoDim[1][1]);

console.log('>>>accessing element in 2 dimensional array (3x2):');
let twoDim2 =[[2,4], [6,8], [10,12]]
console.log(twoDim2[2][0]);

// invoke function using console:

function myName (name){
	console.log ('Hello '+ name.toLowerCase())
}